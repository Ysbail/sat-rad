import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1' # TODO avoid using weak gpu
os.environ['TF_ENABLE_CUBLAS_TENSOR_OP_MATH_FP32'] = '1'
os.environ['TF_ENABLE_CUDNN_TENSOR_OP_MATH_FP32'] = '1'
os.environ['TF_ENABLE_CUDNN_RNN_TENSOR_OP_MATH_FP32'] = '1'

import tensorflow as tf

devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(devices[0], True)

import pandas as pd
import numpy as np

from tqdm import tqdm

import src.models as models
from src.dataset import gen_dataset_sat_rad # gen_dataset_sat_rad_artificial_test # Dataset
from src.trainer import Trainer
from src.grid_tricks import get_chunks

from sklearn.model_selection import train_test_split  
    
# path_generator = PathGenerator() # TODO something to get random paths from satelites and also get glm/topo/radar data needed
epochs = 1000

# dataset = Dataset(path_generator)

'''
# DEBUG MODEL
gen_model = models.gen_model((None, None, 12))
input('verify memory usage of gen')
# gen_model.fit(np.random.rand(32, 128, 128, 12), np.random.rand(32, 128, 128, 1))
# input('verify memory usage of fit')

dis_model = models.gan_discriminator_model((128, 128, 1)) # e.g. (128, 128, 1)
input('verify memory usage of dis')

trainer = Trainer(gen_model, dis_model)
for _ in range(10): trainer.train_step(np.random.rand(4, 128, 128, 12), np.random.rand(4, 128, 128, 1)); print(_)
'''

dates = pd.date_range('2022-01-01', periods=1)
_input, _output = gen_dataset_sat_rad(dates)

# _input, random_state = get_random_chunks(_input, target_dim=(128, 128), return_random=True) # TODO
# _output = get_random_chunks(_output, target_dim=(128, 128), random_points=random_state)

print('out max', np.nanmax(_output))
_where_valid = np.mean(np.isnan(_output), axis=(1, 2, 3)) < 0.3

_input = _input[_where_valid]
_output = _output[_where_valid]

x_train, x_test, y_train, y_test = train_test_split(_input, _output, test_size=0.05)

print(x_train.shape, y_train.shape)

tf.keras.backend.clear_session() # K.clear_session()

gen_model = models.gan_generator_model((None, None, x_train.shape[-1]))
dis_model = models.gan_discriminator_model(y_train.shape[1:]) # e.g. (128, 128, 1)

trainer = Trainer(gen_model, dis_model)

# trainer.use_pre_trained_weights('gen_model', './models/sat_rad_gen_model_bkp.h5')
# trainer.use_pre_trained_weights('dis_model', './models/sat_rad_dis_model_bkp.h5')

_shuffle = np.arange(x_train.shape[0])
np.random.shuffle(_shuffle)

x_train = x_train[_shuffle][:1000] # TODO cropped samples
y_train = y_train[_shuffle][:1000]

batch_size = 16 # arbitrary value that works for 8GB of VRAM
_train_slices = [slice(i * batch_size, (i + 1) * batch_size) for i in range(y_train.shape[0] // batch_size)]

for i in tqdm(range(epochs)):
    for batch in _train_slices:
        trainer.train_step(x_train[batch], y_train[batch])
    
    # save every epoch, it's little time consuming when the dataset is huge
    trainer.gen_model.save('./models/sat_rad_gen_model_bkp.h5')
    trainer.dis_model.save('./models/sat_rad_dis_model_bkp.h5')
    
    if (i + 1) % 1 == 0:
        print(i + 1, trainer.gen_tracker.result(), trainer.dis_tracker.result())
        trainer.plot(x_test, y_test, i)

trainer.gen_model.save('./models/sat_rad_gen_model_v1.h5')
trainer.dis_model.save('./models/sat_rad_dis_model_v1.h5')

