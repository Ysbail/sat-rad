import itertools
import numpy as np
import tensorflow as tf

from sklearn.model_selection import (
    train_test_split,
    StratifiedKFold,
)

def get_chunks(*args, target_dim=(10, 10)):
    out = []
    for arg in args:
        arg = arg.astype(np.float32)
        # arg (batch, lat, lon, feats) or (batch, timesteps, lat, lon, feats)
        ichunks = arg.shape[-3] // target_dim[0]
        jchunks = arg.shape[-2] // target_dim[1]
        islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
        jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
        pre_alloc_arg = np.zeros((*arg.shape[:-3], ichunks * jchunks, *target_dim, arg.shape[-1]), dtype=np.float32)
        for idx, (islice, jslice) in enumerate(itertools.product(islices, jslices)):
            pre_alloc_arg[..., idx, :, :, :] = arg[..., islice, jslice, :]
        if len(pre_alloc_arg.shape) == 6:
            pre_alloc_arg = pre_alloc_arg.transpose((0, 2, 1, 3, 4, 5))
            out.append(pre_alloc_arg.reshape(-1, arg.shape[1], *target_dim, arg.shape[-1]))
        elif len(pre_alloc_arg.shape) == 5:
            out.append(pre_alloc_arg.reshape(-1, *target_dim, arg.shape[-1]))
        else:
            raise 'invalid target shape'
    return out


def get_timed_chunks(*args, target_dim=(10, 10)):
    out = []
    for arg in args:
        # arg (batch, lat, lon, feats) or (batch, timesteps, lat, lon, feats)
        ichunks = arg.shape[-3] // target_dim[0]
        jchunks = arg.shape[-2] // target_dim[1]
        islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
        jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
        pre_alloc_arg = np.zeros((arg.shape[0], ichunks * jchunks, *target_dim, arg.shape[-1]))
        for idx, (islice, jslice) in enumerate(itertools.product(islices, jslices)):
            pre_alloc_arg[..., idx, :, :, :] = arg[..., islice, jslice, :]
        out.append(pre_alloc_arg.transpose((1, 0, 2, 3, 4)))
    return out


def get_random_chunks(_grid, target_dim=(10, 10), input_state=None, return_state=False, n_samples=None):
    random_points = input_state if isinstance(input_state, np.ndarray) else Random(_grid, target=target_dim, n_samples=n_samples).draw_random()
    print(type(random_points))
    islices = [slice(i, i + target_dim[0]) for i in random_points[:, 0]]
    jslices = [slice(j, j + target_dim[1]) for j in random_points[:, 1]]
    pre_alloc_grid = np.zeros((_grid.shape[0], random_points.shape[0], *target_dim, _grid.shape[-1]))
    for idx, (islice, jslice) in enumerate(zip(islices, jslices)):
        pre_alloc_grid[:, idx] = _grid[:, islice, jslice]
    pre_alloc_grid = np.reshape(pre_alloc_grid, (-1, *target_dim, _grid.shape[-1]))
    return (pre_alloc_grid, random_points) if return_state else pre_alloc_grid

class Random:
    def __init__(self, *args, target=(10, 10), n_samples=None):
        if len(args) == 2:
            self.lats = args[0].size
            self.lons = args[1].size
        elif len(args) == 1:
            self.lats = args[0].shape[1]
            self.lons = args[0].shape[2]
            
        self.target = target
        self.n_samples = n_samples or 128 # (self.lats * self.lons) // np.prod(target) # TODO
        
        self.max_lat_idx = self.lats - target[0]
        self.max_lon_idx = self.lons - target[1]
        
        print(self.lats, self.lons, self.target, self.max_lat_idx, self.max_lon_idx)
        
    def draw_random(self, ):
        grid = np.mgrid[0:self.max_lat_idx + 1, 0:self.max_lon_idx + 1]
        points = np.vstack(map(np.ravel, grid)).T
        idx = np.random.choice(range(points.shape[0]), self.n_samples)
        return points[idx, ]

    
def get_stratified_chunks(x, y, target_dim=(10, 10), condition_max_value=[1., 0.5]):
    out = []
    
    ichunks = y.shape[-3] // target_dim[0]
    jchunks = y.shape[-2] // target_dim[1]
    islices = [slice(i * target_dim[0], (i + 1) * target_dim[0]) for i in range(ichunks)]
    jslices = [slice(j * target_dim[1], (j + 1) * target_dim[1]) for j in range(jchunks)]
    
    _x_arg = []
    _y_arg = []
    samples = 0
    for (islice, jslice) in itertools.product(islices, jslices):
        _y_chunk = y[:, islice, jslice, :]
        _condition = (_y_chunk < condition_max_value[0]).mean(axis=(1, 2, 3)) < condition_max_value[1]
        if _condition.sum() > 0:
            _x_arg.append(x[:, islice, jslice, :][_condition])
            _y_arg.append(_y_chunk[_condition])
            samples += _condition.sum()
    if samples < int(ichunks * jchunks * y.shape[0] * 0.3):
        print('less than %d samples, using condition %.2f'%(int(ichunks * jchunks * y.shape[0] * 0.3), condition_max_value[1] + 0.05))
        return get_stratified_chunks(x, y, target_dim, [condition_max_value[0], condition_max_value[1] + 0.05])
    # if samples > 10000:
    #     pick_samples = np.random.choice(np.arange(samples), size=10000, replace=False)
    #     _x_arg = np.concatenate(_x_arg, axis=0)[pick_samples]
    #     _y_arg = np.concatenate(_y_arg, axis=0)[pick_samples]
    #     return (_x_arg, _y_arg)
    return (np.concatenate(_x_arg, axis=0), np.concatenate(_y_arg, axis=0))
    

def get_stratified_shuffle_chunks(x, y, target_dim=(10, 10), target_samples=1000, categories=9):
    y_flat = np.argmax(y, axis=-1).flatten()
    x_flat = np.transpose(x, (0, 2, 3, 1, 4)).reshape(-1, x.shape[1], x.shape[4])
    # clear not enough data
    for cat in range(categories):
        mask_cat = y_flat == cat
        if mask_cat.sum() < 2:
            x_flat = x_flat[~mask_cat]
            y_flat = y_flat[~mask_cat]
        
    skf = StratifiedKFold(2)
    
    for train, valid in skf.split(x_flat, y_flat):
        x_train_singles, x_valid_singles = x_flat[train], x_flat[valid]
        y_train_singles, y_valid_singles = y_flat[train], y_flat[valid]
        break
    
    x_train, x_valid = [], []
    y_train, y_valid = [], []
    for cat in range(categories):
        mask_train = y_train_singles == cat
        mask_valid = y_valid_singles == cat
        if mask_train.sum() == 0 or mask_valid.sum() == 0:
            continue
        
        print('choosen %d samples from %d occurrencies at category %d.'%(target_samples, mask_train.sum(), cat))
        train_choosen = np.random.choice(np.arange(mask_train.sum()), size=target_samples)
        valid_choosen = np.random.choice(np.arange(mask_valid.sum()), size=target_samples)
        
        x_train.append(x_train_singles[mask_train][train_choosen])
        y_train.append(y_train_singles[mask_train][train_choosen])
        x_valid.append(x_valid_singles[mask_valid][valid_choosen])
        y_valid.append(y_valid_singles[mask_valid][valid_choosen])
        
    x_train = np.concatenate(x_train, axis=0)
    y_train = np.concatenate(y_train, axis=0)
    
    # shuffle
    shuffle_train = np.random.shuffle(np.arange(x_train.shape[0]))
    x_train = x_train[shuffle_train]
    y_train = y_train[shuffle_train]
    
    x_train = x_train.reshape(-1, *target_dim, x.shape[1], x.shape[4]).transpose((0, 3, 1, 2, 4))
    y_train = tf.one_hot(y_train.reshape(-1, *target_dim), categories).numpy()
    
    x_valid = np.concatenate(x_valid, axis=0)
    y_valid = np.concatenate(y_valid, axis=0)
    
    # shuffle
    shuffle_valid = np.random.shuffle(np.arange(x_valid.shape[0]))
    x_valid = x_valid[shuffle_valid]
    y_valid = y_valid[shuffle_valid]
    
    x_valid = x_valid.reshape(-1, *target_dim, x.shape[1], x.shape[4]).transpose((0, 3, 1, 2, 4))
    y_valid = tf.one_hot(y_valid.reshape(-1, *target_dim), categories).numpy()
    
    # print('stratify shapes check')
    # print(x_train.shape, y_train.shape, x_valid.shape, y_valid.shape)
    
    # input('press to continue')
    
    return x_train, x_valid, y_train, y_valid


class Chunks:
    def __init__(self, shape_ref, target_dim=(32, 32)):
        self.target_dim = target_dim
        self.ichunks = shape_ref[0] // target_dim[0]
        self.jchunks = shape_ref[1] // target_dim[1]
        self.chunks_per_map = int(self.ichunks * self.jchunks)
        self.remap_shape = (self.ichunks * target_dim[0],
                            self.jchunks * target_dim[1])
        self.prod_slices = None
        
        self.gen_slices()
        
        
    def gen_slices(self, ):
        self.islices = [slice(i * self.target_dim[0], (i + 1) * self.target_dim[0]) for i in range(self.ichunks)]
        self.jslices = [slice(j * self.target_dim[1], (j + 1) * self.target_dim[1]) for j in range(self.jchunks)]
        
        
    def gen_chunks(self, arg):
        pre_alloc_arg = np.zeros((arg.shape[0], self.ichunks * self.jchunks, *self.target_dim, arg.shape[-1]), dtype=np.float32)
        for idx, (islice, jslice) in enumerate(itertools.product(self.islices, self.jslices)):
            pre_alloc_arg[:, idx, :, :, :] = arg[:, islice, jslice, :]
            
        return pre_alloc_arg.reshape(-1, *self.target_dim, arg.shape[-1])
        
        
    def regen_maps(self, chunks):
        num_maps = int(chunks.shape[0] / self.chunks_per_map)
        # print('num maps:', num_maps)
        chunks = chunks.reshape(num_maps, self.chunks_per_map, *self.target_dim, chunks.shape[-1])
        maps = []
        for chunk in chunks:
            # print('chunk shape:', chunk.shape)
            _map = np.zeros((*self.remap_shape, chunks.shape[-1]))
            for c, (islice, jslice) in zip(chunk, itertools.product(self.islices, self.jslices)):
                _map[islice, jslice, :] = c[:, :, :]
            maps.append(_map.copy())
            
        return np.array(maps, dtype=np.float32)
        
        
def get_cluster_indices(grid, initial_index):
    valid_indices = set()
    valid_indices.add(initial_index)
    value = grid[initial_index]
    if value == 0:
        return ()
    grid_cap = ((0, 0), np.array(grid.shape) - 1)
    search_pattern = np.array([[-1, 0], [1, 0], [0, -1], [0, 1]]) # np.vstack(np.split(np.mgrid[-1:2, -1:2].flatten(), 2)).T
    new_indices_to_check = valid_indices.copy()
    while len(new_indices_to_check) > 0:
        new_indices = set()
        for index in new_indices_to_check:
            index = np.array(index)
            for search in search_pattern:
                _new_index = np.clip(index + search, *grid_cap)
                if grid[tuple(_new_index)] == value:
                    new_indices.add(tuple(_new_index))
        valid_indices.update(new_indices_to_check)
        new_indices_to_check = new_indices.difference(valid_indices)
    
    return tuple(map(tuple, np.array(tuple(valid_indices)).T))