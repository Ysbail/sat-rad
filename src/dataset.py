import os, collections

import tensorflow as tf
import numpy as np
import pandas as pd
import netCDF4 as nc

from tqdm import tqdm
from dataclasses import dataclass
from ncBuilder import ncHelper, ncBuilder
from scipy.interpolate import RectBivariateSpline
from src.grid_tricks import get_random_chunks, get_cluster_indices
from scipy.spatial.distance import cdist

import matplotlib.pyplot as plt

CWD = 'C:\\Users\\Eksoss\\work\\sat-rad' # os.getcwd() # '/home/chico/work/sat-rad' # 

@dataclass
class ZR:
    alpha : float = 200.
    beta : float = 1.6
    
    def __call__(self, R):
        return self.alpha * (R ** self.beta)
        
    def inverse(self, Z):
        return (Z / self.alpha) ** (1 / self.beta)

zr = ZR()

_radar_zr = {'AL1': zr,
             'CGC': zr,
             'CHO': zr,
             'GAM': zr,
             'JG1': zr,
             'MC1': zr,
             'MERGE': zr,
             'MIG': zr,
             'NT1': zr,
             'PBR': ZR(32, 1.65),
             'PCO': zr,
             'PE1': zr,
             'SF1': zr,
             'SPZ': ZR(32, 1.5),
             'SR1': zr,
             'ST1': zr,
             'STG': zr,
             'SV1': zr,
             'TM1': zr,
             'WPL': zr,
             'XNG': ZR(32, 1.5),
             'CRJ': ZR(32, 1.65),
             'IMT': ZR(32, 1.5),
             'CHC': zr,
             'LNT': zr,
             'MN1': zr,
             'MCP': zr,
             'SLU': zr,
             'TF1': zr,
             }

@dataclass
class SplineInfo: # not really spline
    lat : np.ndarray
    lon : np.ndarray
    t_lat : np.ndarray
    t_lon : np.ndarray
    lat_slice : slice = None
    lon_slice : slice = None
    
    def __post_init__(self, ):
        imin, jmin = ncHelper.get_idx_pos(np.min(self.t_lat), np.min(self.t_lon), self.lat, self.lon)
        imax, jmax = ncHelper.get_idx_pos(np.max(self.t_lat), np.max(self.t_lon), self.lat, self.lon)
        assert imin < imax
        assert jmin < jmax
        # print('spline info')
        # print(imin, imax, jmin, jmax)
        # print(self.lat.size, self.lon.size)
        self.lat_slice = slice(imin, imax + 1)
        self.lon_slice = slice(jmin, jmax + 1)
        # print(self.lat_slice, self.lon_slice)
    
    def __call__(self, x):
        # print(self.lat[self.lat_slice].size)
        # print(self.lon[self.lon_slice].size)
        # print(x.shape)
        assert x.shape[1] == self.lat_slice.stop - self.lat_slice.start, 'lat input dim wrongly sized'
        assert x.shape[2] == self.lon_slice.stop - self.lon_slice.start, 'lat input dim wrongly sized'
        
        return tf.image.resize(x, [self.t_lat.size, self.t_lon.size], 'bilinear').numpy()
        # return RectBivariateSpline(self.lat[self.lat_slice], self.lon[self.lon_slice], x)(self.t_lat, self.t_lon)


def load_lats_lons(_nc, prefix=None):
    if prefix is not None and (prefix + '_latitude') in _nc.variables.keys():
        _lats = _nc[prefix + '_latitude'][:]
        _lons = _nc[prefix + '_longitude'][:]
    else:
        _lats, _lons = ncHelper.get_lats_lons(_nc)
        
    return _lats, _lons
            
            
def get_variables(_nc):
    if _nc is None:
        return []
    _dims = set(_nc.dimensions)
    _vars = set(_nc.variables)
    return sorted(_vars.difference(_dims))

    
def intersect_time(_ncs):
    _times = {_name: ncHelper.load_time(_nc_dict['data']['time']) for _name, _nc_dict in _ncs.items()}
    _dfs = [pd.DataFrame(np.arange(_time.size), pd.Index(_time), [_name]) for _name, _time in _times.items()]
    return pd.concat(_dfs, axis=1, join='inner')


def _update_spatial_info(_ncs, _dict, prefix=None):
    for _name, _nc_dict in _ncs.items():
        _lats, _lons = load_lats_lons(_nc_dict['data'], prefix)

        _dict['lat_0'].append(np.min(_lats))
        _dict['lat_1'].append(np.max(_lats))
        _dict['lon_0'].append(np.min(_lons))
        _dict['lon_1'].append(np.max(_lons))
        # print('show that res:', max(np.max(np.diff(_lats)), np.max(np.diff(_lons))))
        _dict['res'].append(max(np.max(np.diff(_lats)), np.max(np.diff(_lons))))


def _gen_splines(_ncs, t_lats, t_lons, prefix=None):
    _internal = {}
    for _name, _nc_dict in _ncs.items():
        _lats, _lons = load_lats_lons(_nc_dict['data'], prefix)
        
        _internal[_name] = SplineInfo(_lats, _lons, t_lats, t_lons)
        
    return _internal
    # TODO check LSQSphereBivariateSpline and how to apply, will need positive coordinates, maybe make it in quadrant chunks


def intersect_space(_temporal_ncs, _static_ncs={}, prefix=None, fixed_res=None):
    _ncs = {}
    _ncs.update(_temporal_ncs)
    _ncs.update(_static_ncs)
    
    _spaces = collections.defaultdict(list)
    _update_spatial_info(_ncs, _spaces, prefix)
    
    if fixed_res is None:
        _res = np.max(_spaces['res'])
    else:
        _res = fixed_res
        
    _local_lats = np.arange(np.max(_spaces['lat_0']), np.min(_spaces['lat_1']) + _res / 2, _res)
    _local_lons = np.arange(np.max(_spaces['lon_0']), np.min(_spaces['lon_1']) + _res / 2, _res)
    # print(_local_lats, _local_lons)
    _splines = _gen_splines(_ncs, _local_lats, _local_lons, prefix)
    
    return _splines, _local_lats, _local_lons
    
    
def _handle_nc_var(_nc_var,
                   _time_idx=(slice(None), ),
                   _spline=lambda x: x,
                   t_lat=None,
                   t_lon=None,
                   _mask=None,
                   _filter=lambda x: x,
                   # _is_radar=False,
                   ):
    
    _dims = list(_nc_var.dimensions)
    if not 'time' in _dims: # creates a single axis for compatibility
        _arr = _nc_var[_spline.lat_slice, _spline.lon_slice]
        _arr = _arr[None]
    else:
        _arr = _nc_var[:, _spline.lat_slice, _spline.lon_slice]
    
    # get mask and apply
    if not _mask is None:
        _m_bool = (_mask[:, _spline.lat_slice, _spline.lon_slice] == 1.).astype(float)
        _m_bool[_m_bool == 0.] = np.nan
        _arr *= _m_bool
    
    _arr = _filter(_arr)
    _arr = _arr[_time_idx]
    _arr = _arr[..., None]
    
    _arr = _spline(_arr)

    return _arr


def get_samples(_temporal_ncs, _static_ncs={}, _in_out={}, target_sample_size=128):
    _time_idx = intersect_time(_temporal_ncs)
    _global_input = _global_output = np.empty(0)
    _radar_nc = _temporal_ncs['radar']['data']
    _radars_list = sorted(set(_radar_nc.variables).difference(set(_radar_nc.dimensions)))
    for _radar_key in tqdm(_radars_list[:]): # TODO 
        print(_radar_key)
        if _radar_key in ['PBR_nc', 'MC1_CAPPI']:
            # print('discarded')
            # continue
            pass
            
        _local_spline_funcs, _local_lats, _local_lons = intersect_space(_temporal_ncs, _static_ncs, prefix=_radar_key, fixed_res=3.65320059 / 111)
        
        if _local_lats.size < target_sample_size or _local_lons.size < target_sample_size:
            print('insufficient local dimension size')
            continue
        
        # print('decided locals:', (_local_lats.size, _local_lons.size))
        
        _temporal_array_groups = collections.defaultdict(lambda: np.empty(0))
        _static_array_groups = collections.defaultdict(lambda: np.empty(0))
        
        # load all vars for each temporal nc
        for _name, _nc_dict in _temporal_ncs.items():
            # print(_name)
            _nc = _nc_dict['data']
            _vars = get_variables(_nc)
            
            _nc_mask = _nc_dict.get('mask', lambda x: None)
            if _name == 'radar':
                _vars = [_radar_key]
                
            for _var in _vars:
                _nc_mask_open = _nc_mask(_var)
                _mask_vars = get_variables(_nc_mask_open)
                _arr = _handle_nc_var(_nc[_var],
                                      _time_idx[_name],
                                      _local_spline_funcs[_name],
                                      _local_lats,
                                      _local_lons,
                                      _mask=_nc_mask_open['rain'][:] if 'rain' in _mask_vars else None,
                                      _filter=_nc_dict.get('filter', lambda y: lambda x: x)(_var),
                                      # _is_radar=_name == 'radar',
                                      )
                _temporal_array_groups[_name] = np.concatenate((_temporal_array_groups[_name].reshape(*_arr.shape[:-1], -1), _arr), axis=-1)
        
        for _name, _nc_dict in _static_ncs.items():
            # print(_name)
            _nc = _nc_dict['data']
            _vars = get_variables(_nc)
            # print(_vars)
            for _var in _vars:
                _arr = _handle_nc_var(_nc[_var],
                                      np.index_exp[:],
                                      _local_spline_funcs[_name],
                                      _local_lats,
                                      _local_lons,
                                      _mask=None,
                                      _filter=_nc_dict.get('filter', lambda y: lambda x: x)(_var),
                                      )
                _static_array_groups[_name] = np.concatenate((_static_array_groups[_name].reshape(*_arr.shape[:-1], -1), _arr), axis=-1)
                
        # input
        _temporal_input = np.concatenate([_temporal_array_groups[_name] for _name in _in_out['input']['temporal']], axis=-1)
        if len(_in_out['input']['static']) > 0:
            _static_input = np.concatenate([_static_array_groups[_name] for _name in _in_out['input']['static']], axis=-1)
            _input = np.concatenate((_temporal_input, _static_input.repeat(_temporal_input.shape[0], 0)), axis=-1)
        else:
            _input = _temporal_input

        # output
        _temporal_output = np.concatenate([_temporal_array_groups[_name] for _name in _in_out['output']['temporal']], axis=-1)
        if len(_in_out['output']['static']) > 0:
            _static_output = np.concatenate([_static_array_groups[_name] for _name in _in_out['output']['static']], axis=-1)
            _output = np.concatenate((_temporal_output, _static_output.repeat(_temporal_input.shape[0], 0)), axis=-1)
        else:
            _output = _temporal_output
            
        print('local input shape:', _input.shape)
        print('local output shape:', _output.shape)
        
        _input_discard = np.max(np.min(np.isnan(_input), axis=(1, 2)), axis=-1) # np.isnan(np.nanmax(_input, axis=(1, 2, 3)))
        _output_discard = np.max(np.min(np.isnan(_output), axis=(1, 2)), axis=-1) # np.isnan(np.nanmax(_output, axis=(1, 2, 3)))
        _discard = _input_discard | _output_discard
        
        # _where_invalid = np.max(np.min(np.isnan(_input), axis=(1, 2)), axis=-1) | np.max(np.min(np.isnan(_output), axis=(1, 2)), axis=-1)
        # x_train = x_train[~_where_invalid]
        # y_train = y_train[~_where_invalid]
        
        print('to discard:', _discard.sum())
        
        if _discard.sum() == 144:
            continue
        elif _discard.sum() > 0:
            _input = _input[~_discard]
            _output = _output[~_discard]
        
        '''
        # clear empty samples - no rain cloud
        _bland = np.percentile(_output, 5, axis=(1, 2, 3)) == np.percentile(_output, 95, axis=(1, 2, 3))
        _input = _input[~_bland]
        _output = _output[~_bland]
        '''
        
        # _input, random_state = get_random_chunks(_input, (128, 128), return_state=True, n_samples=8)
        # _output = get_random_chunks(_output, (128, 128), random_state)
        _input, random_state = get_random_chunks(_input, (target_sample_size, target_sample_size), return_state=True, n_samples=16)
        _output = get_random_chunks(_output, (target_sample_size, target_sample_size), random_state)
        
        '''
        # clear too much nan data
        _few_nan = np.mean(np.isnan(_output), axis=(1, 2, 3)) < 0.1
        _input = _input[_few_nan]
        _output = _output[_few_nan]
        '''
        
        print('chunked local input shape:', _input.shape)
        print('chunked local output shape:', _output.shape)
        
        _global_input = np.concatenate((_global_input.reshape(-1, *_input.shape[1:]), _input), axis=0)
        _global_output = np.concatenate((_global_output.reshape(-1, *_output.shape[1:]), _output), axis=0)
        
    return _global_input, _global_output
            

def load_mask(var):
    key = var.split('_')[0]
    try:
        _nc = nc.Dataset(os.path.join(CWD, 'masks/%s_mask.nc'%key), 'r', keepweakref=True)
    except Exception as e:
        print('mask not found:', e)
        _nc = None
    return _nc


def load_filter(var):
    key = var.split('_')[0]
    return _radar_zr[key]
    
    
def gen_dataset_sat_rad(dates, samples_kw={}):

    _input = _output = np.empty(0)

    nc_topo = nc.Dataset(os.path.join(CWD, 'fixed_data/topo.nc'), 'r', keepweakref=True)
    nc_bioma = nc.Dataset(os.path.join(CWD, 'fixed_data/bioma.nc'), 'r', keepweakref=True)
    
    static = {'topo': {'data': nc_topo},
              'bioma': {'data': nc_bioma}}

    _in_out = {'input': {'temporal': ('goes', 'glm'),
                         'static': ('topo', 'bioma')},
               'output': {'temporal': ('radar', ),
                          'static': ()}}
                          
    for date in dates:
        nc_goes  = nc.Dataset(date.strftime('D:\\DATA\\get_goes_run\\DATA\\ABI-L2-MCMIPF_%Y-%m-%d_v2.nc'), 'r', keepweakref=True)
        # nc_goes  = nc.Dataset(date.strftime('/dados2/chico/DATA/g16/goes16/DATA/ABI-L2-MCMIPF_%Y-%m-%d.nc'), 'r', keepweakref=True)
        nc_glm   = nc.Dataset(date.strftime('D:\\DATA\\glm_acu_new_grid\\glm_acu_%Y%m%d.nc'), 'r', keepweakref=True)
        # nc_press = nc.Dataset(date.strftime('press_%Y%m%d.nc'), 'r', keepweakref=True)
        # for radar_name, zr_func in _radar_zr.items():
        nc_radar = nc.Dataset(date.strftime('D:\\DATA\\radar\\%Y%m%d_radar.nc'), 'r', keepweakref=True)
        # nc_radar_mask = nc.Dataset(date.strftime('masks/%s_radar_mask.nc'%radar_name), 'r', keepweakref=True)
        temporal = {'goes': {'data': nc_goes},
                    'glm': {'data': nc_glm},
                    'radar': {'data': nc_radar, 'mask': load_mask, 'filter': load_filter}}

        _temp_input, _temp_output = get_samples(temporal, _static_ncs=static, _in_out=_in_out, **samples_kw)
        # temporal as sample concatenation
        _input = np.concatenate((_input.reshape(-1, *_temp_input.shape[1:]), _temp_input), axis=0)
        _output = np.concatenate((_output.reshape(-1, *_temp_output.shape[1:]), _temp_output), axis=0)
        
    return _input, _output


if __name__ == '__main__':
    # _input, _output = gen_dataset_sat_rad_artificial_test(pd.date_range('2021-01-01', periods=3))
    _input, _output = gen_dataset_sat_rad(pd.date_range('2022-03-01', periods=1))
    print(_input.shape, _output.shape)
    
    input()
    
    print(type(_output))
    # yhat = _output[0, ..., 0]
    plt.pcolormesh(np.log10(_output[0, ..., 0].clip(1., None))*10.)
    plt.colorbar()
    plt.show()
        
'''
def get_samples(_temporal_ncs, _static_ncs={}, _in_out={}):
    _time_idx = intersect_time(_temporal_ncs) # pd.DataFrame
    # TODO include regional data for each radar
    _global_input = _global_output = np.empty(0)
    _radar_nc = _temporal_ncs.get('radar').get('data')
    _valid_vars = list(set(_radar_nc.variables).difference(set(_radar_nc.dimensions)))
    for _radar_key in _valid_vars:
        _spline_funcs, _global_lats, _global_lons = intersect_space(_temporal_ncs, _static_ncs, prefix=_radar_key)
        _temporal_array_groups = collections.defaultdict(lambda: np.empty(0))
        _static_array_groups = collections.defaultdict(lambda: np.empty(0))

        for _name, _nc_dict in _temporal_ncs.items():
            _nc = _nc_dict['data']
            _vars = get_variables(_nc)
            
            _nc_mask = _nc_dict.get('mask', lambda x: None)
            # _mask_vars = get_variables(_nc_mask)
             
            for _var in _vars:
                _mask_vars = get_variables(_nc_mask(_var))
                _arr = _handle_nc_var(_nc[_var],
                                      _time_idx[_name],
                                      _spline_funcs[_name],
                                      _global_lats,
                                      _global_lons,
                                      _mask=_nc_mask[_var][:] if _var in _mask_vars else None,
                                      _filter=_nc_dict.get('filter', lambda y: lambda x: x)(_var),
                                      )
                _temporal_array_groups[_name] = np.concatenate((_temporal_array_groups[_name].reshape(*_arr.shape[:-1], -1), _arr), axis=-1)
                
        for _name, _nc_dict in _static_ncs.items():
            _nc = _nc_dict['data']
            _vars = get_variables(_nc)
            
            _nc_mask = _nc_dict.get('mask', None)
            _mask_vars = get_variables(_nc_mask)
                
            for _var in _vars:
                _arr = _handle_nc_var(_nc[_var],
                                      np.index_exp[:],
                                      _spline_funcs[_name],
                                      _global_lats,
                                      _global_lons,
                                      _mask=_nc_mask[_var][:] if _var in _mask_vars else None,
                                      _filter=_nc_dict.get('filter', lambda y: lambda x: x)(_var),
                                      )
                _static_array_groups[_name] = np.concatenate((_static_array_groups[_name].reshape(*_arr.shape[:-1], -1), _arr), axis=-1)

        # input
        _temporal_input = np.concatenate([_temporal_array_groups[_name] for _name in _in_out['input']['temporal']], axis=-1)
        if len(_in_out['input']['static']) > 0:
            _static_input = np.concatenate([_static_array_groups[_name] for _name in _in_out['input']['static']], axis=-1)
            _input = np.concatenate((_temporal_input, _static_input.repeat(_temporal_input.shape[0], 0)), axis=-1)
        else:
            _input = _temporal_input

        # output
        _temporal_output = np.concatenate([_temporal_array_groups[_name] for _name in _in_out['output']['temporal']], axis=-1)
        if len(_in_out['output']['static']) > 0:
            _static_output = np.concatenate([_static_array_groups[_name] for _name in _in_out['output']['static']], axis=-1)
            _output = np.concatenate((_temporal_output, _static_output.repeat(_temporal_input.shape[0], 0)), axis=-1)
        else:
            _output = _temporal_output
    
    _global_input = np.r_[_global_input.reshape(-1, *_input.shape[1:]), _input]
    _global_output = np.r_[_global_output.reshape(-1, *_output.shape[1:]), _output]
    
    return _global_input, _global_output    
'''


'''
def gen_dataset_sat_rad_artificial_test(dates):
    import matplotlib.pyplot as plt
    _2km_res = 0.2 # 2 / 111
    _500m_res = 0.05 # .5 / 111
    
    _lat_2km = np.arange(-50, 10 + _2km_res / 2, _2km_res)
    _lon_2km = np.arange(-80, -20 + _2km_res / 2, _2km_res)
    
    _lat_500m = np.arange(-50, 10 + _500m_res / 2, _500m_res)
    _lon_500m = np.arange(-80, -20 + _500m_res / 2, _500m_res)
    
    _time_1h = pd.date_range('2021-01-01', freq='H', periods=24).to_pydatetime()
    _time_3h = pd.date_range('2021-01-01', freq='3H', periods=8).to_pydatetime()

    _level = np.arange(1000, 500, -50)
    
    _input = _output = np.empty(0)

    nc_topo = nc.Dataset('topo.nc', 'w', diskless=True)
    ncBuilder.create_nc(nc_topo, _lat_2km, _lon_2km, vars={'topo': {'dims': ('latitude', 'longitude')}})
    ncBuilder.update_nc(nc_topo, 'topo', np.random.rand(_lat_2km.size, _lon_2km.size))
    
    static = {'topo': {'data': nc_topo}}

    _in_out = {'input': {'temporal': ('goes', 'glm', 'press', ),
                         'static': ('topo', )},
               'output': {'temporal': ('radar', ),
                          'static': ()}}
                          
    for date in dates:
        nc_goes  = nc.Dataset(date.strftime('goes_%Y%m%d.nc'),  'w', diskless=True)
        ncBuilder.create_nc(nc_goes,
                            _lat_2km,
                            _lon_2km,
                            time=_time_1h,
                            vars={'ch%02d'%(i + 7): {'dims': ('time',
                                                              'latitude',
                                                              'longitude')}
                                  for i in range(10)})
        [ncBuilder.update_nc(nc_goes,
                             'ch%02d'%(i + 7),
                             np.random.rand(_time_1h.size,
                                            _lat_2km.size,
                                            _lon_2km.size)) for i in range(10)]
        
        nc_glm   = nc.Dataset(date.strftime('glm_%Y%m%d.nc'), 'w', diskless=True)
        ncBuilder.create_nc(nc_glm,
                            _lat_2km,
                            _lon_2km,
                            time=_time_1h,
                            vars={'glm': {'dims': ('time',
                                                   'latitude',
                                                   'longitude')}})
        ncBuilder.update_nc(nc_glm, 'glm', np.random.rand(_time_1h.size,
                                                          _lat_2km.size,
                                                          _lon_2km.size))
        
        nc_press = nc.Dataset(date.strftime('press_%Y%m%d.nc'), 'w', diskless=True)
        ncBuilder.create_nc(nc_press,
                            _lat_2km,
                            _lon_2km,
                            time=_time_1h,
                            level=_level,
                            vars={'press': {}})
        ncBuilder.update_nc(nc_press,
                            'press',
                            np.random.rand(_time_1h.size,
                                           _level.size,
                                           _lat_2km.size,
                                           _lon_2km.size))
        
        nc_radar = nc.Dataset(date.strftime('radar_%Y%m%d.nc'), 'w', diskless=True)
        ncBuilder.create_nc(nc_radar,
                            _lat_500m,
                            _lon_500m,
                            time=_time_3h,
                            vars={'radar': {'dims': ('time',
                                                     'latitude',
                                                     'longitude')}})
        ncBuilder.update_nc(nc_radar,
                            'radar',
                            10**(np.random.rand(_time_3h.size,
                                               _lat_500m.size,
                                               _lon_500m.size) * 7.5))
                                           
        nc_radar_mask = nc.Dataset(date.strftime('radar_mask_%Y%m%d.nc'), 'w', diskless=True)
        ncBuilder.create_nc(nc_radar_mask,
                            _lat_500m,
                            _lon_500m,
                            time=_time_3h[:1],
                            vars={'radar': {'dims': ('time',
                                                     'latitude',
                                                     'longitude')}})
        _arr = np.arange(_lat_500m.size * _lon_500m.size).reshape(1, _lat_500m.size, _lon_500m.size)
        _arr = np.cos(_arr)
        _arr[_arr > 0] = 1
        _arr[_arr <= 0] = 0
        ncBuilder.update_nc(nc_radar_mask,
                            'radar',
                            _arr)

        temporal = {'goes': {'data': nc_goes},
                    'glm': {'data': nc_glm},
                    'press': {'data': nc_press},
                    'radar': {'data': nc_radar, 'mask': nc_radar_mask}}  

        print('ncs created')
        _temp_input, _temp_output = get_samples(temporal, _static_ncs=static, _in_out=_in_out)
        print('samples gotten')
        # temporal as sample concatenation
        
        print(_temp_input.shape, _temp_output.shape)
        
        _input = np.concatenate((_input.reshape(-1, *_temp_input.shape[1:]), _temp_input), axis=0)
        _output = np.concatenate((_output.reshape(-1, *_temp_output.shape[1:]), _temp_output), axis=0)
        
        print(_input.shape, _output.shape)
    
    return _input, _output
'''


    
"""
@dataclass
class SubNC:
    nc_file : nc.Dataset
    times : np.ndarray = None
    lats : np.ndarray = None
    lons : np.ndarray = None
    
    def __post_init__(self, ):
        if 'time' in nc_file.dimensions:
            self.times = ncHelper.load_time(nc_file['time'])
        self.lats, self.lons = ncHelper.get_lats_lons(nc_file)
        
    def crop(self, _var, t_lats, t_lons):
        _grids = self.nc_file.variables[_var]
        if self.times is None:
            _grids = _grids[None, ]
        return np.array([RectBivariateSpline(self.lats, self.lons, _grid)(t_lats, t_lons) for _grid in _grids])
        
    def time_crop(self, ):
        
        pass
        
        

class Dataset:
    def __init__(self, nc_files_dict, **kwargs):
        self.keys = []
        for k, v in nc_files_dict.items()
            setattr(self, k, v)
            self.keys.append(k)
            
        # self.input_path = _input_path
        # self.output_path = _output_path
        # self.dates = np.asanyarray(_dates)
        
        self.x_train = None
        self.y_train = None
        self.x_test = None
        self.y_test = None
        
        self._range_lats = kwargs.get('_range_lats', np.array([-90., 90.]))
        self._range_lons = kwargs.get('_range_lons', np.array([-180., 180.]))
        
        # self.methods = ['bilinear', 'lanczos3', 'lanczos5', 'bicubic', 'gaussian', 'nearest', 'area', 'mitchellcubic']
        self.methods = ['lanczos3', ]
        self.x_scaler = None
        self.y_scaler = None
        
        # default for cfs x merge
        # obsolete
        self.in_slice = slice(None) # kwargs.get('in_slice', slice(None, 4))
        self.out_slice = slice(None) # kwargs.get('out_slice', slice(None, 24, 6))
        
        self.in_var = kwargs.get('in_var', 'precipitation')
        self.out_var = kwargs.get('out_var', 'precipitation')
        # for merge x merge parse slice(None) for both
        
        self.in_filter = kwargs.get('in_filter', lambda x: np.clip(x, 0., 100.)) # Callable
        self.out_filter = kwargs.get('out_filter', lambda x: np.clip(x, 0., 100.)) # Callable
        
        self.in_target_dim = kwargs.get('in_target_dim', (32, 32))
        self.out_target_dim = kwargs.get('out_target_dim', (128, 128))
        
        self.in_agg = kwargs.get('in_agg', False)
        self.out_agg = kwargs.get('out_agg', False)
        
        self.input_crop = None
        self.output_crop = None
        
        self.idx_map = None
        
        self._eval()


    def _eval(self, ):
        _date = self.dates[0]
        
        # evaluate the coordinates
        _input_lats, _input_lons, _output_lats, _output_lons = self.load_latlons(_date)
        
        self._range_lats[0] = max([_input_lats.min(), _output_lats.min(), self._range_lats[0]])
        self._range_lats[1] = min([_input_lats.max(), _output_lats.max(), self._range_lats[1]])
        self._range_lons[0] = max([_input_lons.min(), _output_lons.min(), self._range_lons[0]])
        self._range_lons[1] = min([_input_lons.max(), _output_lons.max(), self._range_lons[1]])
                
        _lat_step = np.diff(_output_lats).mean().round(2)
        _lon_step = np.diff(_output_lons).mean().round(2)
        
        print('steps:', _lat_step, _lon_step)
        
        self.general_lats = np.arange(self._range_lats[0], self._range_lats[1] + _lat_step / 2, _lat_step)
        self.general_lons = np.arange(self._range_lons[0], self._range_lons[1] + _lon_step / 2, _lon_step)
        
        _lat_slice = slice(None, self.general_lats.size // self.out_target_dim[0] * self.out_target_dim[0])
        _lon_slice = slice(None, self.general_lons.size // self.out_target_dim[1] * self.out_target_dim[1])
        
        self.general_lats = self.general_lats[_lat_slice]
        self.general_lons = self.general_lons[_lon_slice]
        
        print('generals:', self.general_lats.shape, self.general_lons.shape)
        
        self.output_crop = (self.out_slice,
                            slice(np.argmin(abs(_output_lats - self.general_lats[0])), np.argmin(abs(_output_lats - self.general_lats[0])) + self.general_lats.size),
                            slice(np.argmin(abs(_output_lons - self.general_lons[0])), np.argmin(abs(_output_lons - self.general_lons[0])) + self.general_lons.size))
        self.input_crop = (self.in_slice,
                           slice(np.argmin(abs(_input_lats - self.general_lats[0])), np.argmin(abs(_input_lats - self.general_lats[0])) + self.general_lats.size),
                           slice(np.argmin(abs(_input_lons - self.general_lons[0])), np.argmin(abs(_input_lons - self.general_lons[0])) + self.general_lons.size))
                           
        print('crops:', self.input_crop, self.output_crop)
        self.input_resize = lambda x, method: tf.image.resize(x, [self.general_lats.size // 4, self.general_lons.size // 4], method).numpy()


    def _load_idx_map(self, _date):
        _in_nc_files = [nc.Dataset(path.format(date=_date), 'r', keepweakref=True) for path in self.input_path]
        _out_nc_files = [nc.Dataset(path.format(date=_date), 'r', keepweakref=True) for path in self.output_path]
        _nc_files = [*_in_nc_files, *_out_nc_files]
        
        _times = [pd.to_datetime(ncHelper.load_time(_nc_file['time'])) for _nc_file in _nc_files]
        _dfs = [pd.DataFrame(np.arange(_time.size), index=pd.Index(_time)) for _time in _times]
        self.idx_map = pd.concat(_dfs, axis=1, join='inner')

        [_nc_file.close() for _nc_file in _nc_files]


    @staticmethod
    def _handle_multiple_paths(paths, _var, _crop, _idx_map, _head_time_idx=0, _agg=False, **kwargs):
        _nc_files = [nc.Dataset(path, 'r', keepweakref=True) for path in paths]
        
        _array = []
        for idx, _nc_file in enumerate(_nc_files):
            _valid_idx = _idx_map.iloc[:, idx + _head_time_idx]
            if _agg: # gambi para resolver problema de agregação de variáveis (soma de precipitação)
                _sub_arr = []
                for _idx in _valid_idx:
                    _sub_arr.append(_nc_file[_var][_idx - kwargs.get('agg_range', 6): _idx + 1].sum(axis=0, keepdims=True)[_crop][..., None]) # adds a channel dimension
                _arr = np.concatenate(_sub_arr, axis=0)
            else:
                _arr = _nc_file[_var][_valid_idx][_crop][..., None] # adds a channel dimension
            _array.append(_arr)
            
        _array = np.concatenate(_array, axis=-1)
        
        [_nc_file.close() for _nc_file in _nc_files]
        
        return _array


    def load_data(self, _date):
        self._load_idx_map(_date)
        
        _array_input = self._handle_multiple_paths([_path.format(date=_date) for _path in self.input_path], self.in_var, self.input_crop, self.idx_map, _agg=self.in_agg)
        _array_output = self._handle_multiple_paths([_path.format(date=_date) for _path in self.output_path], self.out_var, self.output_crop, self.idx_map, len(self.input_path), _agg=self.out_agg)

        return _array_input, _array_output


    def load_latlons(self, _date):
        _input_lats, _input_lons = ncHelper.get_lats_lons(self.input_path[0].format(date=_date), 1, 0)
        _output_lats, _output_lons = ncHelper.get_lats_lons(self.output_path[0].format(date=_date), 1, 0)
        
        _input_lons[_input_lons > 180] -= 360
        _output_lons[_output_lons > 180] -= 360
        
        return _input_lats, _input_lons, _output_lats, _output_lons


    def process_data(self, _date):
        _x, _y = self.load_data(_date)
        _x = np.concatenate([self.input_resize(_x, method) for method in self.methods], axis=-1)
        
        _x = self.in_filter(_x)
        _y = self.out_filter(_y)
        
        # get chunks
        _x = grid_tricks.get_chunks(_x, target_dim=self.in_target_dim)
        _y = grid_tricks.get_chunks(_y, target_dim=self.out_target_dim)
        
        _x = _x.reshape((-1, *_x.shape[-3:]))
        _y = _y.reshape((-1, *_y.shape[-3:]))
        
        return _x, _y


    def _prepare_curr_dataset(self, ):
        self.x_train = []
        self.y_train = []
        
        for _date in tqdm(self.dates):
            _x_temp, _y_temp = self.process_data(_date)
            self.x_train.append(_x_temp)
            self.y_train.append(_y_temp)
            
        self.x_train = np.concatenate(self.x_train, axis=0)
        self.y_train = np.concatenate(self.y_train, axis=0)
        
        print(self.x_train.shape)
        print(self.y_train.shape)
        
        self.x_scaler = MinMaxSpatial((-1., 1.))
        self.y_scaler = MinMaxSpatial((-1., 1.))
        
        self.x_scaler.fit(self.x_train)
        self.y_scaler.fit(self.y_train)
        
        self.x_train = self.x_scaler.transform(self.x_train)
        self.y_train = self.y_scaler.transform(self.y_train)
        
        print('x_train shape', self.x_train.shape)
        print('y_train shape', self.y_train.shape)
        
        _test_size = 32
        self.x_test = self.x_train[-_test_size:]
        self.y_test = self.y_train[-_test_size:]
        
        self.x_train = self.x_train[:-_test_size]
        self.y_train = self.y_train[:-_test_size]
        
          
class MinMaxSpatial:
    def __init__(self, feature_range, clip=True):
        self.feature_range = feature_range
        self.clip = clip
        self._min = None
        self._data_min = None
        self._data_max = None
        self._data_range = None
        self._data_scale = None
        
        
    def fit(self, X):
        _shape = tuple(np.arange(np.ndim(X) - 1, dtype=np.uint8))
        self._data_min = np.min(X, axis=_shape, keepdims=True)
        self._data_max = np.max(X, axis=_shape, keepdims=True)
        self._data_range = self._data_max - self._data_min
        self._data_scale = (self.feature_range[1] - self.feature_range[0]) / self._data_range
        self._min = self.feature_range[0] - self._data_min * self._data_scale
        
        print('fitting MinMaxSpatial:', self._data_scale, self._min)
        
        
    def transform(self, X):
        X = X.copy()
        X *= self._data_scale
        X += self._min
        if self.clip:
            np.clip(X, self.feature_range[0], self.feature_range[1], out=X)
        return X
        
        
    def inverse_transform(self, X):
        X = X.copy()
        X -= self._min
        X /= self._data_scale
        return X
        
        
"""
