import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.use("Agg")

from src.losses import (
    mae_loss,
    mse_loss,
    binary_loss,
    categorical_loss,
    its_a_fake_loss_func,
    true_jedi_loss_func,
    psnr_loss_func,
)


class Trainer:
    def __init__(self, gen_model, dis_model, **kwargs):
        self.gen_model = gen_model
        self.dis_model = dis_model
        
        self.gen_model = _Model(gen_model, kwargs.get('gen_opt', tf.keras.optimizers.Adam(1e-4)), tf.keras.metrics.Mean(name='gen_loss'))
        self.dis_model = _Model(dis_model, kwargs.get('dis_opt', tf.keras.optimizers.Adam(1e-4)), tf.keras.metrics.Mean(name='dis_loss'))
        
    def compile(self, loss='mse'):
        self.gen_model.model.compile(loss=loss, optimizer=self.gen_model.opt)
        self.dis_model.model.compile(loss=loss, optimizer=self.dis_model.opt)
        
    def load_model(self, _type, _path):
        _model = tf.keras.models.load_model(_path)
        setattr(self, _type + '_model', _Model(_model, _model.optimizer, tf.keras.metrics.Mean(name=_type + '_loss')))
        
    def use_pre_trained_weights(self, _model, _path):
        getattr(self, _model).load_weights(_path)
    
    def train_step(self, lr, hr):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as dis_tape:
            sr = self.gen_model(lr, training=True)
            
            hr_predict = self.dis_model(hr, training=True)
            sr_predict = self.dis_model(sr, training=True)
            
            gen_sr_loss = mse_loss(hr, sr)
            gen_psnr_loss = -psnr_loss_func(hr, sr)
            
            gen_loss = 1e-3 * true_jedi_loss_func(sr_predict) + gen_psnr_loss + gen_sr_loss # seeks how to deceive the discriminator
            dis_loss = its_a_fake_loss_func(sr_predict) + true_jedi_loss_func(hr_predict)
            
            # print('gen_loss', gen_loss)
            # print('dis_loss', dis_loss)
        
        self.gen_model.optimize(gen_tape, gen_loss)
        self.dis_model.optimize(dis_tape, dis_loss)
        
    def train_step_categorical(self, lr, hr):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as dis_tape:
            sr = self.gen_model(lr, training=True)
            
            hr_predict = self.dis_model(hr, training=True)
            sr_predict = self.dis_model(sr, training=True)
            zero_pred = self.dis_model(tf.one_hot(np.zeros((1, *hr.shape[1:3])), hr.shape[-1]).numpy(), training=True)
            
            _decieve = 1e-2 * true_jedi_loss_func(sr_predict)
            gen_sr_loss = categorical_loss(hr, sr)
            gen_loss = _decieve + gen_sr_loss
            
            _bad_wine = its_a_fake_loss_func(sr_predict)
            _good_wine = true_jedi_loss_func(hr_predict)
            _tasteless = its_a_fake_loss_func(zero_pred)
            dis_loss = _bad_wine + _good_wine + _tasteless
            
            # print('gen_dis_loss * 1e-3', _decieve)
            # print('gen_sr_loss', gen_sr_loss)
            # print('fake loss', _bad_wine)
            # print('true loss', _good_wine)
        
        self.gen_model.optimize(gen_tape, gen_loss)
        self.dis_model.optimize(dis_tape, dis_loss)
        
    def train_step_ignore_hr(self, lr, hr):
        with tf.GradientTape() as gen_tape:
            sr = self.gen_model(lr, training=True)
            
            sr_predict = self.dis_model(sr, training=False)
            
            gen_sr_loss = mse_loss(hr, sr)
            gen_psnr_loss = -psnr_loss_func(hr, sr)
            
            gen_loss = 1e-3 * true_jedi_loss_func(sr_predict) + gen_psnr_loss + gen_sr_loss # seeks how to deceive the discriminator
        
        self.gen_model.optimize(gen_tape, gen_loss)
        
    def plot(self, x_test, y_test, idx, prefix='test'):
        _extras = 4
        _rows = min(x_test.shape[0], 32)
        _cols = x_test.shape[-1] + _extras
        lr = x_test
        _sr = []
        for i in range(8):
            _sr.append(self.gen_model.predict(x_test[i * 4: (i + 1) * 4]))
        sr = np.concatenate(_sr, axis=0)
        hr = y_test
        
        fig = plt.figure(figsize=(7. * _cols, 7. * _rows))
        for i in range(_rows):
            for j in range(_cols - _extras):
                plt.subplot(_rows, _cols, i * _cols + j + 1)
                plt.pcolormesh(lr[i, ..., j], cmap='cividis') # , vmin=-1., vmax=1.)
                plt.colorbar()
                
            # extra cols
            plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 1))
            plt.pcolormesh(np.argmax(hr[i], axis=-1) * 5, cmap='cividis', shading='auto', vmin=0., vmax=65.)
            plt.colorbar()
            
            plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 2))
            plt.pcolormesh(np.argmax(sr[i], axis=-1) * 5, cmap='cividis', shading='auto', vmin=0., vmax=65.)
            plt.title('hard categorical')
            # plt.title('mse loss: %f'%mse_loss(hr[i], sr[i]).numpy() + '\npsnr loss: %f'%(-psnr_loss_func(hr[i], sr[i])))
            plt.colorbar()
            
            plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 3))
            plt.pcolormesh(np.dot(sr[i], np.arange(sr.shape[-1])) / sr[i].sum(-1) * 5, cmap='cividis', shading='auto', vmin=0., vmax=65.)
            plt.title('soft categorical')
            # plt.title('mse loss: %f'%mse_loss(hr[i], sr[i]).numpy() + '\npsnr loss: %f'%(-psnr_loss_func(hr[i], sr[i])))
            plt.colorbar()
            
            plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 4))
            plt.pcolormesh((np.argmax(sr[i, ..., 1:], axis=-1) + 1) * 5, cmap='cividis', shading='auto', vmin=0., vmax=65.)
            plt.title('hard categorical above 0')
            # plt.title('mse loss: %f'%mse_loss(hr[i], sr[i]).numpy() + '\npsnr loss: %f'%(-psnr_loss_func(hr[i], sr[i])))
            plt.colorbar()
            
            # plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 3))
            # plt.pcolormesh(sr_den[i, ..., 0], cmap='cividis', vmin=-1., vmax=1.)
            # plt.colorbar()
            
            # plt.subplot(_rows, _cols, (i + 1) * _cols - (_extras - 4))
            # plt.pcolormesh(sr_den[i, ..., 0] - sr[i, ..., 0], cmap='bwr', vmin=-1., vmax=1.)
            # plt.colorbar()
            
        plt.savefig(f'./imgs/{prefix}_plot_batch_%02d.jpg'%(idx + 1))
        plt.close()
    
     
class _Model:
    def __init__(self, model, opt, tracker):
        self.model = model
        self.opt = opt
        self.tracker = tracker
        
        self.vars = self.model.trainable_variables
        
        
    def optimize(self, tape, loss):
        _grad = tape.gradient(loss, self.vars)
        self.opt.apply_gradients(zip(_grad, self.vars))
        self.tracker.update_state(loss)
    
    
    # ugly funcs but works
    def save(self, *args, **kwargs):
        self.model.save(*args, **kwargs)
    
    
    def load_weights(self, *args, **kwargs):
        self.model.load_weights(*args, **kwargs)
        
        
    def predict(self, *args, **kwargs):
        return self.model.predict(*args, **kwargs)
    
    
    def __call__(self, *args, **kwargs):
        return self.model(*args, **kwargs)
        
        
