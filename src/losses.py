import tensorflow as tf
from tensorflow.keras.losses import (
    MeanSquaredError,
    MeanAbsoluteError,
    BinaryCrossentropy,
    CategoricalCrossentropy,
)

binary_loss = BinaryCrossentropy(from_logits=False)
categorical_loss = CategoricalCrossentropy(from_logits=False)
# mse_loss = MeanSquaredError()
# mae_loss = MeanAbsoluteError()

def mae_loss(y_true, y_pred):
    _is_nan = tf.math.is_nan(y_true) | tf.math.is_nan(y_pred) | (y_true == -1.)
    _t = tf.where(_is_nan, -1., y_true)
    _p = tf.where(_is_nan, -1., y_pred)
    return MeanAbsoluteError()(_t, _p)


def mse_loss(y_true, y_pred):
    _is_nan = tf.math.is_nan(y_true) | tf.math.is_nan(y_pred) | (y_true == -1.)
    _t = tf.where(_is_nan, -1., y_true)
    _p = tf.where(_is_nan, -1., y_pred)
    return MeanSquaredError()(_t, _p)


def its_a_fake_loss_func(_fake):
    loss = binary_loss(tf.zeros_like(_fake), _fake)
    return loss


def true_jedi_loss_func(_true):
    loss = binary_loss(tf.ones_like(_true), _true)
    return loss


def psnr_loss_func(y_true, y_pred):
    y_true = tf.cast(y_true, 'float32')
    y_pred = tf.cast(y_pred, 'float32')
    return -10. * tf.math.log(mse_loss(y_pred, y_true) + 1.) / tf.math.log(tf.constant(10., 'float32'))
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
