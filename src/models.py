import tensorflow as tf

from tensorflow.keras.models import Model

from tensorflow.keras.layers import (
    Add,
    Dense,
    Input,
    PReLU,
    Conv2D,
    Flatten,
    LeakyReLU,
    Concatenate,
    MaxPooling2D,
    SeparableConv2D,
    SpatialDropout2D,
    BatchNormalization,
    )
    
from tensorflow.keras.initializers import (
    Constant,
    RandomNormal,
    )
    

def _conv_norm_prelu(x, **conv_kw):
    _x = Conv2D(**conv_kw)(x)
    _x = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_x)
    _x = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(_x)
    return _x

    
def gen_model(input_shape=(None, None, 1), categories=1, loss='mse', optimizer='adam'):
    _in = Input(input_shape)
    _bn = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_in)
    
    _c_in = SeparableConv2D(filters=64,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            depth_multiplier=8,
                            padding='same',
                            activation='linear',
                            kernel_initializer=RandomNormal(0., 0.02),
                            )(_bn)
    _c_in = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(_c_in)

    _jump_connection = _c_in
    for _ in range(8): _c_in = _inception_residual(_c_in)
    
    _add = Conv2D(filters=64,
                           kernel_size=(3, 3),
                           strides=(1, 1),
                           # depth_multiplier=8,
                           padding='same',
                           activation='linear',
                           kernel_initializer=RandomNormal(0., 0.02),
                           )(_c_in)
    _add = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_add)
    
    _jump_out = Add()([_jump_connection, _add])
    _c_out = Conv2D(filters=categories,
                             kernel_size=(3, 3),
                             strides=(1, 1),
                             # depth_multiplier=8,
                             padding='same',
                             activation='softmax',
                             kernel_initializer=RandomNormal(0., 0.02),
                             )(_jump_out)
    
    model = Model(_in, _c_out)
    # model.compile(loss=loss, optimizer=optimizer)
    return model


def _inception_residual(x):
    _c_1 = _conv_norm_prelu(x, 
                            filters=32,
                            kernel_size=(1, 1),
                            strides=(1, 1),
                            padding='same',
                            activation='linear')

    _c_3_1 = _conv_norm_prelu(x, 
                              filters=32,
                              kernel_size=(1, 1),
                              strides=(1, 1),
                              padding='same',
                              activation='linear')
    _c_3 = _conv_norm_prelu(_c_3_1,
                            filters=32,
                            kernel_size=(3, 3),
                            strides=(1, 1),
                            padding='same',
                            activation='linear')
    
    _c_5_1 = _conv_norm_prelu(x,
                              filters=32,
                              kernel_size=(1, 1),
                              strides=(1, 1),
                              padding='same',
                              activation='linear')
    _c_5 = _conv_norm_prelu(_c_5_1,
                            filters=32,
                            kernel_size=(5, 5),
                            strides=(1, 1),
                            padding='same',
                            activation='linear')
                 
    _mp = MaxPooling2D((3, 3), (1, 1), padding='same')(x)
    _c_mp = _conv_norm_prelu(_mp,
                             filters=32,
                             kernel_size=(1, 1),
                             strides=(1, 1),
                             padding='same',
                             activation='linear')
    
    # dropouts
    _c_1 = SpatialDropout2D(0.25)(_c_1)
    _c_3 = SpatialDropout2D(0.25)(_c_3)
    _c_5 = SpatialDropout2D(0.25)(_c_5)
    _c_mp = SpatialDropout2D(0.25)(_c_mp)
    
    # reworking the inception information into a single residual information
    _concat = Concatenate()([_c_1, _c_3, _c_5, _c_mp])
    _add = Conv2D(filters=64,
                           kernel_size=(3, 3),
                           strides=(1, 1),
                           # depth_multiplier=8,
                           padding='same',
                           activation='linear')(_concat)
    _add = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_add)
    # _add = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(_add)
    
    _out = Add()([x, _add])
    
    return _out


def gan_conv_bn_leaky(_nn, filters, kernel, strides):
    _nn = Conv2D(filters=filters,
                 kernel_size=kernel,
                 strides=strides,
                 padding='same',
                 activation='linear',
                 kernel_initializer=RandomNormal(0., 0.02))(_nn)
    _nn = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_nn)
    _nn = LeakyReLU(0.2)(_nn)
    return _nn
    
        
def gan_discriminator_model(input_shape, optimizer='adam', loss='mse'):
    '''
    For training it needs to have a fixed input_shape, not (None, None, c)
    It won't work as the last Dense layer won't have a reference which to
    create the kernel matrix.
    
    '''
    
    unknown_inputs = Input(input_shape)
    n = unknown_inputs
    
    n = Conv2D(filters=64,
               kernel_size=(4, 4),
               strides=(2, 2),
               padding='same',
               activation='linear',
               kernel_initializer=RandomNormal(0., 0.02))(n)
    n = LeakyReLU(0.2)(n)
    
    n = gan_conv_bn_leaky(n, 128, 4, 2)
    n = gan_conv_bn_leaky(n, 256, 4, 2)
    n = gan_conv_bn_leaky(n, 512, 4, 2)
    n = gan_conv_bn_leaky(n, 1024, 4, 2)
    n = gan_conv_bn_leaky(n, 2048, 4, 2)
    n = gan_conv_bn_leaky(n, 1024, 1, 1)
    nn = gan_conv_bn_leaky(n, 512, 1, 1)
    
    n = gan_conv_bn_leaky(nn, 128, 1, 1)
    n = gan_conv_bn_leaky(n, 128, 3, 1)
    n = Conv2D(filters=512,
               kernel_size=(3, 3),
               strides=(1, 1),
               padding='same',
               activation='linear',
               kernel_initializer=RandomNormal(0., 0.02))(n)
    n = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(n)
    
    n = Add()([nn, n])
    n = LeakyReLU(0.2)(n)
    
    n = Flatten()(n)
    n = Dense(1, activation='sigmoid')(n)
    
    model = Model(unknown_inputs, n)
    # model.compile(optimizer=optimizer, loss=loss)
    
    return model






def srgan_upsampling(_nn):
    # _nn = Lambda(lambda x: tf.pad(x, ((0, 0), (1, 1), (1, 1), (0, 0)), 'REFLECT'))(_nn) # added
    _nn = Conv2D(filters=256, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='linear', kernel_initializer=RandomNormal(0., 0.02))(_nn) # modified
    _nn = Lambda(lambda x: tf.nn.depth_to_space(x, block_size=2))(_nn)
    _nn = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(_nn)
    return _nn


def srgan_conv_bn(_nn, kernel_size=(3, 3)):
    # _nn = Lambda(lambda x: tf.pad(x, ((0, 0), (1, 1), (1, 1), (0, 0)), 'REFLECT'))(_nn) # added
    _nn = Conv2D(filters=64, kernel_size=kernel_size, strides=(1, 1), padding='same', activation='linear', kernel_initializer=RandomNormal(0., 0.02))(_nn) # modified
    _nn = BatchNormalization(gamma_initializer=RandomNormal(1., 0.02))(_nn)
    return _nn


def srgan_res_block(_n):
    _nn = srgan_conv_bn(_n)
    _nn = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(_nn)
    _nn = srgan_conv_bn(_nn)
    _nn = Add()([_n, _nn])
    return _nn
    
    
def gen_model_res(input_shape=(None, None, 1), categories=1, optimizer='adam', loss='mse'):
    lr_inputs = Input(input_shape)
    
    # include hrnet pre-processing?
    n = lr_inputs
    # n = Lambda(lambda x: tf.pad(x, ((0, 0), (4, 4), (4, 4), (0, 0)), 'REFLECT'))(lr_inputs) # added
    if input_shape[-1] > 1:
        n = SeparableConv2D(filters=64,
                            kernel_size=(9, 9),
                            strides=(1, 1),
                            depth_multiplier=25,
                            padding='same',
                            activation='linear',
                            kernel_initializer=RandomNormal(0., 0.02))(n) # modified
    else:
        n = Conv2D(filters=64,
                   kernel_size=(9, 9),
                   strides=(1, 1),
                   padding='same',
                   activation='linear',
                   kernel_initializer=RandomNormal(0., 0.02))(n)
                   
    n = PReLU(alpha_initializer=Constant(0.25), shared_axes=(1, 2))(n)
    
    skip_connect = n
    
    for _ in range(5): n = srgan_res_block(n)
    
    n = srgan_conv_bn(n)
    n = Add()([skip_connect, n])
    
    # for _ in range(2): n = srgan_upsampling(n)
    
    # n = Lambda(lambda x: tf.pad(x, ((0, 0), (4, 4), (4, 4), (0, 0)), 'REFLECT'))(n) # added
    
    out = Conv2D(filters=categories,
                 kernel_size=(9, 9),
                 strides=(1, 1),
                 padding='same',
                 activation='softmax',
                 kernel_initializer=RandomNormal(0., 0.02))(n) # modified
    
    '''
    out = SeparableConv2D(filters=1,
                          kernel_size=(9, 9),
                          strides=(1, 1),
                          depth_multiplier=4,
                          padding='same',
                          activation='tanh',
                          kernel_initializer=RandomNormal(0., 0.02))(n) # modified
    '''
    model = Model(lr_inputs, out)
    # model.compile(optimizer=optimizer, loss=loss)
    
    return model