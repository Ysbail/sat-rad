import netCDF4 as nc
import numpy as np
import pandas as pd

from ncBuilder import (
    ncBuilder,
    ncHelper,
    )

class ClassierStuff:
    def __init__(self, *args, groups, data_format, **kwargs):
        self.dims = list(kwargs.get('dims', ('time', 'level', 'latitude', 'longitude')))
        self._super_ncs = list(map(lambda x: SomethingClassy(x, dims=self.dims), args))
        self.groups = groups
        self.data_format = data_format
        
        self._target_transpose = {}
        self._target_space = {}
        
        self._target_expand = {}
        self._target_time_indices = []
        
        self._build_transpose()
        self._build_time_intersect()
        # self._build_spatial_intersect()
        self._build_groups()
        
    def _build_transpose(self, ):
        for group, conditions in self.data_format.items():
            _source_dims = self.dims.copy()
            _target_dims = conditions['dims']
            
            self._target_transpose[group] = tuple([_source_dims.index(_t) for _t in _target_dims])
            
    def _build_time_intersect(self, ):
        _times = [_super_nc.time for _super_nc in self._super_ncs if _super_nc.is_temporal]
        _valids = iter(list(range(len(_times))))
        _valid_times = [_valids.__next__() if _super_nc.is_temporal else None for _super_nc in self._super_ncs]
        _dfs = [pd.DataFrame(np.arange(_time.size), index=pd.Index(_time)) for _time in _times]
        
        _df = pd.concat(_dfs, axis=1, join='inner')
        self._target_time_indices = [(tuple(_df.iloc[:, _valid]), ) if _valid is not None else (slice(None), ) for _valid in _valid_times]
        
    def _build_groups(self, ):
        for group in self.groups: setattr(self, group, np.empty(0))
        for _super_nc_idx, _super_nc in enumerate(self._super_ncs):
            for group, _vars in _super_nc.vars.items():
                _super_nc_array = np.empty(0)
                for _var in _vars:
                    _array = _super_nc._load_var(_var)
                    
                    # TODO check if some dont have temporal intersection ?
                    if _super_nc.is_temporal:
                        _array = _array[self._target_time_indices[_super_nc_idx]] # apply time intersect
                    else:
                        pass
                    
                    # TODO apply spatial correction ?
                    if _super_nc.is_spatial:
                        _array = _array
                    else:
                        pass
                    
                    _array = np.transpose(_array, self._target_transpose[group]) # apply dimensions correction
                    _super_nc_array = np.concatenate((_super_nc_array.reshape(*_array.shape[:-1], -1), _array), axis=-1) # change channels?
                setattr(self, group, np.concatenate((getattr(self, group).reshape(*_super_nc_array.shape[:-1], -1), _super_nc_array), axis=-1))


class SomethingClassy: # SuperNC
    def __init__(self, info, **kwargs):
        self._info = info
    
        self.is_temporal = False
        self.is_spatial = False
        self.temporal_dim = 0
        self.spatial_dims = (0, 1)
        
        self.time = None
        self.lats = None
        self.lons = None
        
        self.nc_file = None
        self.vars = None
        
        self._target_dims = list(kwargs.get('dims', ('time', 'level', 'latitude', 'longitude')))
    
        self._load_info()
        
    def _load_info(self, ):
        self._load_file(self._info.get('path', self._info.get('nc_file', None)))
        # TODO solve for non temporal or non spatial data
        if 'time' in self.nc_file.dimensions.keys():
            self.time = pd.to_datetime(ncHelper.load_time(self.nc_file['time']))
            self.is_temporal = True
            _dims = list(self.nc_file.dimensions)
            self.temporal_dim = _dims.index('time')
            
        if all(map(self.nc_file.dimensions.keys().__contains__, ['latitude', 'longitude'])):
            self.lats, self.lons = ncHelper.get_lats_lons(self.nc_file)
            self.is_spatial = True
            _dims = list(self.nc_file.dimensions)
            self.spatial_dims = (_dims.index('latitude'), _dims.index('longitude'))
            
        self.vars = self._info['vars']
        self.rules = self._info.get('rules', {})
        
    def _load_file(self, _file):
        if isinstance(_file, str):
            self.nc_file = nc.Dataset(_file)
        elif isinstance(_file, nc.Dataset):
            self.nc_file = _file
        else:
            raise TypeError
            
    def _load_var(self, _var):
        _variable = self._resolve_variable_dims(self.nc_file[_var])
        _filter = self.rules.get(_var, lambda x: x)
        
        return _filter(_variable)
        
    def _resolve_variable_dims(self, nc_variable):
        _source_dims = list(nc_variable.dimensions)
        _nc_array = np.asanyarray(nc_variable)

        for _t_dim in self._target_dims:
            if not _t_dim in _source_dims:
                _source_dims.append(_t_dim)
                _nc_array = _nc_array[..., None]
        
        _target_transpose = tuple(_source_dims.index(_t_dim) for _t_dim in self._target_dims)
        _nc_array = np.transpose(_nc_array, _target_transpose)
        
        return _nc_array
        
SuperNC = SomethingClassy


if __name__ == '__main__':
    lat, lon = np.arange(-30, 10, 1.), np.arange(-60, -10, 1.)
    time_0 = pd.date_range('2021-01-01', periods=50)
    time_1 = pd.date_range('2021-02-01', periods=50)

    nc_file_0 = nc.Dataset('a.nc', 'w', diskless=True, keepweakref=True)
    nc_file_1 = nc.Dataset('b.nc', 'w', diskless=True, keepweakref=True)

    _v_0 = ['var1', 'var2', 'var3']
    _v_1 = ['var2', 'var3', 'var4', 'var5']

    _vars_0 = {k : {'dims': ('time', 'latitude', 'longitude')} for k in _v_0}
    _vars_1 = {k : {} for k in _v_1}

    ncBuilder.create_nc(nc_file_0, lat, lon, time=time_0, vars=_vars_0)
    ncBuilder.create_nc(nc_file_1, lat, lon, time=time_1, vars=_vars_1)

    _fill_0 = lambda: np.random.rand(time_0.size, lat.size, lon.size)
    _fill_1 = lambda: np.random.rand(time_0.size, 1, lat.size, lon.size)

    for _var in _v_0: ncBuilder.update_nc(nc_file_0, _var, _fill_0())
    for _var in _v_1: ncBuilder.update_nc(nc_file_1, _var, _fill_1())

    info_0 = {'nc_file': nc_file_0, 'vars': {'input': ['var1', 'var2']}}
    info_1 = {'nc_file': nc_file_1, 'vars': {'input': ['var2', 'var4']}}

    dataset = ClassierStuff(info_0, info_1, groups=['input'], data_format={'input': {'dims': ('time', 'latitude', 'longitude', 'level')}})

    print(dataset.input.shape)
